/* p.17 */
Chapter 2. Data Types

PostgreSQL categorizes data into several data types to store.  Sample databases so far have utilized the following three data types:
* numeric data type
* character data type
* date/time data type
In this chapter, we will learn data types Postgre can treat and their characteristics.

2.1 Numeric Data Types

Numeric data types are for storing "numbers".  In databases, numeric data can be easily handled by built-in functions for arithmetic and other numeric operations.

2.1.1 integer type

Integer type is the data type for integers.  It can store from -2147483648 to +2147483647.  Integer type does not store fractional numbers, fractional parts will be rounded off.
As the following example shows, if you INSERT the value of 30.4 to an integer type data, then 30 will be stored with the fractional part being rounded off.  Then, if you UPDATE it to 30.5, 31 will be stored after rounded off.

[fig.]

/* p.18 */
2.1.2 numeric type

Numeric type is the data type for numbers of any precision.  It can store numbers with fractional part.  It can store numbers with up to 1,000 digits consisting of integral and fractional parts.  The total number of digits combining integral and fractional parts is called "precision", the number of fractional digits is called "scale", and they will be specified as follows:

[ numeric(precision, scale) ]

For example, if you specify as numeric(6,2), then the total number of digits will be set to 6, fractional digits will be 2, therefore integral digits will be 6 - 2 = 4, so it can store a number up to 9999.99.

In the following example, the table numeric_test has a column id of numeric type.  Since its integral part being set to four digits, an INSERT command specifying 19999 makes an error since the number has five integral digits.

[fig.]

2.1.3 other numeric types

In addition to integer type and numeric type, PostgreSQL is equipped with other numeric data types.  For example, decimal type is equivalent to numeric type.  They are provided for the purpose of data characteristic description or database compatibility.

/* part of this section from p.19 */
[Table 2.1  Numeric Data Types ]
+----------------+--------+-------------------------------------------------+
|Data type       |Size    |Range                                            |
+----------------+--------+-------------------------------------------------+
|smallint        |2 bytes |from -32768 to +32767                            |
|                |        |                                                 |
|integer         |4 bytes |from -2147483648 to +2147483647                  |
|                |        |                                                 |
|bigint          |8 bytes |from -9223372036854775808 to +9223372036854775807|
|                |        |                                                 |
|decimal         |variable|1000 digits at maximum                           |
|                |        |                                                 |
|numeric         |variable|1000 digits at maximum                           |
|                |        |                                                 |
|real            |4 bytes |6 digits precision                               |
|                |        |                                                 |
|double precision|8 bytes |15 digits precision                              |
|                |        |                                                 |
|serial          |4 bytes |from 1 to 2147483647                             |
|                |        |                                                 |
|bigserial       |8 bytes |from 1 to 9223372036854775807                    |
+----------------+--------+-------------------------------------------------+


/* p.18 continued */
2.2 Character Data Types

Character data types store character string data.  While they are not furnished with calculations like numeric types, instead, they are equipped with operators like LIKE, which performs pattern matching.

/* p.19 */
2.2.1 character varying type (varchar type)

Varchar type is a variable length character string type with an upper limit for the number of characters.  Being variable length, it can store character strings of any length up to the limit.  Attempt to store characters exceeding the upper limit length makes an error.
In the following example, the table varchar_test has the varchar type of length 3.  A character string of length exceeding 3 makes an error, irrespective of character coding scheme.

[fig.]

2.2.2 character type (char type)

Char type is a fixed length character string type with the upper limit for the number of characters.  Remaining places will be filled with blanks.  Attempt to store characters exceeding the upper limit makes an error.
In the following example, the table char_test has the char type of length 3.  A character string of length exceeding 3 makes an error, irrespective of character coding scheme.

[fig.]

2.2.3 text type

Text type is a variable length character string type without an upper limit of the number of characters.  It would be convenient since it's not necessary to specify character length, however, text type is not defined in ANSI SQL Standard.

2.3 Date/Time Data Types

There are three date/time data types:  those which store date or time separately, and one which stores both.  You can choose one at your convenience.  Usages of Date/Time data types are described later.

2.3.1 date type

Date type stores date only.  Use it if you don't need time information.

2.3.2 time type

Time type stores time only.  Use it if you don't need date information.

2.3.3 timestamp type

Timestamp type stores both date and time.

/* End of Chapter 2 */
